# Simple Autoloader in PHP5

**Author: Lukas Cerny**

**Contact: Ja.LukasCerny@gmail.com**

## Example:

	
	//vendor/sources/debugger/debugger.php
	namespace Sources\Debugger\Debugger;
	class Debugger {...}
	
	//index.php
	require_once __DIR__ . '/Autoloader.php';
	$autoloader = Classes\Autoloader::getInstance();
	
	$autoloader->addNamespace('Sources\\', __DIR__ . '/vendor/sources');
	
	
"