<?php
namespace Classes;

use Exception;
/**
 * Simple autoloader for autoload classes and interfaces.
 *
 * @version v2.0
 * @author Lukas Cerny <Ja.LukasCerny@gmail.com>
 *
 */
class Autoloader
{

    private static $instance = null;

    private $regNamespace = array();

    private function __construct()
    {
    	if (!function_exists('spl_autoload_register')) {
    		throw new Exception('Function spl_autoload_register not exist in this PHP instalation.');
        }
        spl_autoload_register(array($this, 'loadClass'), true);
    }

    /**
     * Returns singleton instance.
     * @return Autoloader
     */
    public static function getInstance()
    {
        if (self::$instance == null)
            self::$instance = new Autoloader();
        return (self::$instance);
    }

    /**
     * @example $namespace = 'A\\B'
     * 			$location = '\var\www\Vendor\Library\src\'
     *
     * @param string $namespace
     * @param string $location
     */
    public function addNamespace($namespace, $location)
    {
        $path = realpath($location) . '/';
        $temp = "";
        foreach (explode('\\', $namespace) as $value) {
        	if (strlen($value)) {
        		$temp .= $value . '\\';
        	}
        }
        $namespace = substr($temp, 0, -1);
        var_dump($namespace);
        if (! array_key_exists($namespace, $this->regNamespace))
            $this->regNamespace[$namespace] = $path;
    }

    /**
     *
     * @param string $class
     */
    public function loadClass($class)
    {
    	$namespace = $class;
    	while(!array_key_exists($namespace, $this->regNamespace)) {
    		$temp = explode('\\', $namespace);
    		array_pop($temp);
    		$namespace = implode('\\', $temp);
    		if(!count($temp)) break;
    	}
    	if (array_key_exists($namespace, $this->regNamespace)) {
    		$path = $this->regNamespace[$namespace];
    		$temp2 = explode('\\', $namespace);
    		$temp = explode('\\', $class);
    		for ($i = count($temp2); $i < count($temp); $i++){
				$path .= $temp[$i];
				if ($i + 1 < count($temp))
					$path .= '/';
    		}
    		require_once $path . '.php';
    	}
    }

    /**
     *
     * @param string $namespace
     * @return string
     */
    public function parseClass($class)
    {
    	$temp = str_replace('\\', DIRECTORY_SEPARATOR, $class);
    	$path = explode('/', $temp);
    	$file = array_pop($path);
    	return (array('path' => implode('/', $path), 'file' => $file));
    }
}